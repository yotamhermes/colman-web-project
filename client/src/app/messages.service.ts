import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { BehaviorSubject } from 'rxjs';
import { Config, uniqueNamesGenerator, starWars } from 'unique-names-generator';
import { Message } from './chat/message';
import { HttpClient, HttpParams } from '@angular/common/http';

const config: Config = {
  dictionaries: [starWars],
};
@Injectable({
  providedIn: 'root',
})
export class MessagesService {
  url = 'http://localhost:3001/messages';

  public sender: string = uniqueNamesGenerator(config);

  private messagesSource = new BehaviorSubject([]);
  public messages = this.messagesSource.asObservable();

  public filters: any = {};

  constructor(private socket: Socket, public http: HttpClient) {
    socket.on('message', (msg: Message) => {
      this.getMessages();
    });

    this.getMessages();
  }

  updateFilter(filters: object) {
    this.filters = { ...this.filters, ...filters };

    this.getMessages();
  }

  getMessages(): void {
    let params = new HttpParams();
    if (this.filters.search) {
      params = params.set('search', this.filters.search);
    }

    if (this.filters.sender) {
      params = params.set('sender', this.filters.sender);
    }

    this.http
      .get<Message[]>(`${this.url}`, {
        params,
      })
      .subscribe((data) => this.messagesSource.next(data as any));
  }

  sendMessage(msg: string) {
    const message: Message = {
      message: msg,
      sender: this.sender,
    };

    this.socket.emit('message', message);
  }

  deleteMessage(id: string) {
    let params = new HttpParams();
    params = params.set('_id', id);

    this.http
      .delete<string>(`${this.url}`, {
        params,
      })
      .subscribe(() => {
        this.socket.emit('message', {
          sender: this.sender,
        });
        this.getMessages();
      });
  }

  getWordFrequency(word: string, callback: Function) {
    let params = new HttpParams();
    params = params.set('word', word);

    this.http
      .get<Message[]>(`${this.url}/word-frequency`, {
        params,
      })
      .subscribe((data: any) => callback(data.count));
  }
}
