import { Venue } from './../venue/venue';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { VenuesService } from '../venues.service';
import { VenueType } from '../venue/venueType';

@Component({
  selector: 'update-venue-dialog',
  templateUrl: './update-venue-dialog.component.html',
  styleUrls: ['./update-venue-dialog.component.css'],
})
export class UpdateVenueDialogComponent implements OnInit {
  @ViewChild('name') name: any;
  @ViewChild('type') type: any;
  @ViewChild('rating') rating: any;
  @ViewChild('address') address: any;
  venuesTypes: VenueType[] = [];

  ratings: number[] = [1, 2, 3, 4, 5];
  constructor(
    private venuesService: VenuesService,
    public dialogRef: MatDialogRef<UpdateVenueDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Venue
  ) {}

  ngOnInit(): void {
    this.venuesService.venuesTypes.subscribe(
      (venuesTypes) => (this.venuesTypes = venuesTypes)
    );
  }

  onUpdate() {
    const venue = {
      _id: this.data._id,
      name: this.name.nativeElement.value,
      type: this.type.nativeElement.value,
      rating: parseInt(this.rating.nativeElement.value),
      address: this.address.nativeElement.value,
    };

    this.venuesService.updateVenue(venue);
    this.dialogRef.close();
  }
}
