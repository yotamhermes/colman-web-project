import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateVenueDialogComponent } from './update-venue-dialog.component';

describe('UpdateVenueDialogComponent', () => {
  let component: UpdateVenueDialogComponent;
  let fixture: ComponentFixture<UpdateVenueDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateVenueDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateVenueDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
