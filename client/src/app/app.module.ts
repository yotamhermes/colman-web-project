import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { MenuComponent } from './menu/menu.component';
import { VenuesComponent } from './venues/venues.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { MapComponent } from './map/map.component';
import { VenuComponent } from './venue/venue.component';
import { venuesFilterComponent } from './venues-filter/venues-filter.component';
import { VenueTypeFilterComponent } from './venue-type-filter/venue-type-filter.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ChatComponent } from './chat/chat.component';
import { VenueRateFilterComponent } from './venue-rate-filter/venue-rate-filter.component';
import { AddVenueComponent } from './add-venue/add-venue.component';
import { MatDialogModule } from '@angular/material/dialog';
import { AddVenueDialogComponent } from './add-venue-dialog/add-venue-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { UpdateVenueDialogComponent } from './update-venue-dialog/update-venue-dialog.component';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { WordFrequencyComponent } from './word-frequency/word-frequency.component';

const config: SocketIoConfig = { url: 'http://localhost:3001', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    MenuComponent,
    VenuesComponent,
    StatisticsComponent,
    MapComponent,
    VenuComponent,
    venuesFilterComponent,
    VenueTypeFilterComponent,
    ChatComponent,
    VenueRateFilterComponent,
    AddVenueComponent,
    AddVenueDialogComponent,
    UpdateVenueDialogComponent,
    BarChartComponent,
    WordFrequencyComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    SocketIoModule.forRoot(config),
    MatDialogModule,
    MatIconModule,
  ],
  providers: [Title],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
