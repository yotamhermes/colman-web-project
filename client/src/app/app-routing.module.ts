import { ChatComponent } from './chat/chat.component';
import { VenuesComponent } from './venues/venues.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'statistics', component: StatisticsComponent },
  { path: 'venues', component: VenuesComponent },
  { path: 'chat', component: ChatComponent },
  { path: '', redirectTo: '/venues', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
