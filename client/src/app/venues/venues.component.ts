import { Component, OnInit } from '@angular/core';
import { Venue } from '../venue/venue';
import { VenuesService } from '../venues.service';
import {} from 'googlemaps';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-venues',
  templateUrl: './venues.component.html',
  styleUrls: ['./venues.component.css'],
})
export class VenuesComponent implements OnInit {
  @ViewChild('map', { static: true }) mapElement: any;
  map: google.maps.Map | undefined;
  geocoder: google.maps.Geocoder | undefined;
  venues: Venue[] = [];
  markers: google.maps.Marker[] = [];

  constructor(private venuesService: VenuesService) {}

  ngOnInit(): void {
    this.venuesService.venues.subscribe((venues) => {
      this.venues = venues;

      this.updateMarkers(venues);
    });

    const mapProperties = {
      center: new google.maps.LatLng(31.0461, 34.8516),
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };

    this.map = new google.maps.Map(
      this.mapElement.nativeElement,
      mapProperties
    );

    this.geocoder = new google.maps.Geocoder();
    
    this.updateMarkers(this.venues)
  }

  updateMarkers(venues: Venue[]): void {
    this.markers.map((x) => x.setMap(null));
    // const newMarkers: google.maps.Marker[] = [];

    // venues.map((venue) => {
    //   this.geocoder?.geocode({ address: venue.address }, (results) => {
    //     const marker = new google.maps.Marker({
    //       position: results[0].geometry.location,
    //       title: venue.name,
    //       label: venue.name,
    //       map: this.map,
    //     });
    //     newMarkers.push(marker);
    //   });

    //   this.markers = newMarkers;
    // });

    this.markers = this.venues.map((venue) => {
      const marker = new google.maps.Marker({
        position: this.randomLatLang(),
        title: venue.name,
        label: venue.name,
        map: this.map,
      });

      return marker;
    });
  }

  randomLatLang(): google.maps.LatLng {
    const lat = Math.random() * (32.0461 - 31) + 31;
    const long = Math.random() * (35.8516 - 34.3) + 34.3;

    console.log(lat, long);

    return new google.maps.LatLng(lat, long);
  }
}
