export interface VenuesFilter {
  type?: string | null;
  search?: string | null;
  rating?: number | null;
}
