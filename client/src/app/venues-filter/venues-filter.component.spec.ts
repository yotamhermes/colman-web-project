import { ComponentFixture, TestBed } from '@angular/core/testing';

import { venuesFilterComponent } from './venues-filter.component';

describe('venuesFilterComponent', () => {
  let component: venuesFilterComponent;
  let fixture: ComponentFixture<venuesFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ venuesFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(venuesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
