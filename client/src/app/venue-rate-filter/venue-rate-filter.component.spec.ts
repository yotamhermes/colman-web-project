import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VenueRateFilterComponent } from './venue-rate-filter.component';

describe('VenueRateFilterComponent', () => {
  let component: VenueRateFilterComponent;
  let fixture: ComponentFixture<VenueRateFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VenueRateFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VenueRateFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
