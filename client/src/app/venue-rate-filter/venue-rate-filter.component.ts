import { Component, OnInit } from '@angular/core';
import { VenuesService } from '../venues.service';

@Component({
  selector: 'venue-rate-filter',
  templateUrl: './venue-rate-filter.component.html',
  styleUrls: ['./venue-rate-filter.component.css'],
})
export class VenueRateFilterComponent implements OnInit {
  ratings: number[] = [1, 2, 3, 4, 5];
  constructor(private venuesService: VenuesService) {}

  ngOnInit(): void {}

  onOptionsSelected(value: string) {
    const rating: number = parseInt(value);
    
    this.venuesService.updateFilters({
      rating,
    });
  }
}
