import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VenueTypeFilterComponent } from './venue-type-filter.component';

describe('VenueTypeFilterComponent', () => {
  let component: VenueTypeFilterComponent;
  let fixture: ComponentFixture<VenueTypeFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VenueTypeFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VenueTypeFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
