import { Component, OnInit } from '@angular/core';
import { VenueType } from '../venue/venueType';
import { VenuesService } from '../venues.service';

@Component({
  selector: 'venue-type-filter',
  templateUrl: './venue-type-filter.component.html',
  styleUrls: ['./venue-type-filter.component.css'],
})
export class VenueTypeFilterComponent implements OnInit {
  venuesTypes: VenueType[] = [];

  constructor(private venuesService: VenuesService) {}

  ngOnInit(): void {
    this.getVenuesTypes();
  }

  getVenuesTypes(): void {
    this.venuesService.venuesTypes.subscribe(
      (venuesTypes) => (this.venuesTypes = venuesTypes)
    );
  }

  onOptionsSelected(type: string) {
    this.venuesService.updateFilters({
      type,
    });
  }
}
