import { VenuesFilter } from './venues-filter/venues-filter';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Venue } from './venue/venue';

@Injectable({
  providedIn: 'root',
})
export class VenuesService {
  private venuesUrl = 'http://localhost:3001/venues';

  private venuesSource = new BehaviorSubject([]);
  public venues = this.venuesSource.asObservable();

  private groupedVenuesSource = new BehaviorSubject([]);
  public groupedVenues = this.groupedVenuesSource.asObservable();

  private venuesTypesSource = new BehaviorSubject([]);
  public venuesTypes = this.venuesTypesSource.asObservable();

  public filters: VenuesFilter = {
    search: null,
    type: null,
  };

  private groupByField = 'type';

  constructor(private http: HttpClient) {
    this.getVenues();
    this.getVenuesTypes();
    this.getGroupedVenues();
  }

  updateFilters(filters: VenuesFilter): void {
    this.filters = { ...this.filters, ...filters };

    this.getVenues();
  }

  getVenues(): void {
    let params = new HttpParams();

    if (this.filters.type) {
      params = params.set('type', this.filters.type);
    }

    if (this.filters.search) {
      params = params.set('search', this.filters.search);
    }

    if (this.filters.rating) {
      params = params.set('rating', this.filters.rating);
    }

    this.http
      .get<Venue[]>(`${this.venuesUrl}`, {
        params,
      })
      .subscribe((data) => this.venuesSource.next(data as any));
  }

  getVenuesTypes(): void {
    this.http
      .get<string[]>(`${this.venuesUrl}/types`)
      .subscribe((data) => this.venuesTypesSource.next(data as any));
  }

  deleteVenue(_id: string): void {
    let params = new HttpParams();
    params = params.set('_id', _id);

    this.http
      .delete<string>(`${this.venuesUrl}`, {
        params,
      })
      .subscribe(() => this.getVenues());
  }

  addVenue(venue: any): void {
    this.http
      .post<string>(`${this.venuesUrl}`, venue)
      .subscribe(() => this.getVenues());
  }

  updateVenue(venue: Venue): void {
    this.http
      .put<string>(`${this.venuesUrl}`, venue)
      .subscribe(() => this.getVenues());
  }

  getGroupedVenues(): void {
    let params = new HttpParams();
    params = params.set('groupByField', 'type')
    this.http
      .get<string>(`${this.venuesUrl}/groupedVenues`, {params})
      .subscribe((data) => this.groupedVenuesSource.next(data as any));
  }
}
