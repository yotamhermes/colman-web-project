import { VenuesService } from './../venues.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  constructor(private venuesService: VenuesService) {}

  ngOnInit(): void {}

  onKey(event: any): void {   
    const ENTER_KEY = 'Enter';
    
    // if submit
    if (event.key == ENTER_KEY) {
      this.venuesService.updateFilters({
        search: event.target.value,
      });
    }
  }
}
