import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddVenueDialogComponent } from '../add-venue-dialog/add-venue-dialog.component';

@Component({
  selector: 'add-venue',
  templateUrl: './add-venue.component.html',
  styleUrls: ['./add-venue.component.css'],
})
export class AddVenueComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  openDialog() {
    const dialogRef = this.dialog.open(AddVenueDialogComponent);
  }
}
