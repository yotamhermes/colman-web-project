export interface Message {
    _id?: string;
    sender?: string;
    message?: string;
}