import { MessagesService } from './../messages.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit {
  messages: any[] = [];
  @ViewChild('messagesContainer') messagesContainer: any;
  sender: string = '';

  constructor(private messagesService: MessagesService) {
    this.sender = messagesService.sender;
  }

  ngOnInit(): void {
    this.messagesService.messages.subscribe((messages) => {
      this.messages = messages;

      setTimeout(() => {
        this.messagesContainer.nativeElement.scrollTop =
          this.messagesContainer.nativeElement.scrollHeight;
      }, 10);
    });
  }

  onMessageKey(event: any): void {
    const ENTER_KEY = 'Enter';

    // if submit
    if (event.key == ENTER_KEY) {
      this.sendMessage(event.target.value);
      event.target.value = '';
    }
  }

  sendMessage(message: string): void {
    this.messagesService.sendMessage(message);
  }

  onDelete(id: string) {
    this.messagesService.deleteMessage(id);
  }

  onSearchKey(event: any): void {
    const ENTER_KEY = 'Enter';

    // if submit
    if (event.key == ENTER_KEY) {
      this.messagesService.updateFilter({
        search: event.target.value
      });
    }
  }

  onSenderKey(event: any): void {
    const ENTER_KEY = 'Enter';

    // if submit
    if (event.key == ENTER_KEY) {
      this.messagesService.updateFilter({
        sender: event.target.value
      });
    }
  }
}
