import { Component, OnInit } from '@angular/core';
import { VenuesService } from '../venues.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css'],
})
export class StatisticsComponent implements OnInit {
  groupByFields: string[] = [];
  field: string;
  groupedVenues: any[] = [];

  constructor(private venuesService: VenuesService) {
    this.groupByFields = ['type', 'rating'];
    this.field = 'type';
  }

  ngOnInit(): void {
    this.venuesService.groupedVenues.subscribe((groupedVenues) => {
      this.groupedVenues = groupedVenues;
      console.log(this.groupedVenues);
    });
  }
}
