import { MessagesService } from './../messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'word-frequency',
  templateUrl: './word-frequency.component.html',
  styleUrls: ['./word-frequency.component.css'],
})
export class WordFrequencyComponent implements OnInit {
  count: number | undefined;
  constructor(public messagesService: MessagesService) {}

  ngOnInit(): void {}

  onKey(event: any): void {
    const ENTER_KEY = 'Enter';

    // if submit
    if (event.key == ENTER_KEY) {
      this.messagesService.getWordFrequency(
        event.target.value,
        (count: number) => (this.count = count)
      );
    }
  }
}
