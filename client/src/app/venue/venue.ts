import { VenueType } from "./venueType";

export interface Venue {
  _id: string;
  logoUrl?: string;
  name: string;
  type: VenueType;
  address: string;
  rating: number;
}