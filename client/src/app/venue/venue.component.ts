import { UpdateVenueDialogComponent } from './../update-venue-dialog/update-venue-dialog.component';
import { Input } from '@angular/core';
import { Component } from '@angular/core';
import { Venue } from './venue';
import { VenuesService } from '../venues.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'venue',
  templateUrl: './venue.component.html',
  styleUrls: ['./venue.component.css'],
})
export class VenuComponent {
  @Input() venue!: Venue;

  constructor(private venuesService: VenuesService, public dialog: MatDialog) {}

  onUpdate(): void {
    const dialogRef = this.dialog.open(UpdateVenueDialogComponent, {
      data: this.venue,
    });
  }

  onDelete(): void {
    this.venuesService.deleteVenue(this.venue._id);
  }
}
