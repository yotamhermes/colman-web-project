import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { VenueType } from '../venue/venueType';
import { VenuesService } from '../venues.service';

@Component({
  selector: 'bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {

  groupedVenues: any[] = [];
  venuesTypes: VenueType[] = [];

  private svg: any;
  private margin = 130;
  private width = 1200 - (this.margin * 2);
  private height = 500 - (this.margin * 2);

  constructor(private venuesService: VenuesService) { }

  ngOnInit(): void {
    this.createSvg();
    this.venuesService.groupedVenues.subscribe((groupedVenues) => {
      this.groupedVenues = groupedVenues;

      this.venuesService.venuesTypes.subscribe((venuesTypes) => {
          this.venuesTypes = venuesTypes;
          if(this.groupedVenues.length != 0){
            this.drawBars(this.groupedVenues)
          }
        } 
      );
    });
  }

  ngAfterContentInit():void{
    
  }

  private createSvg(): void {
    this.svg = d3.select("figure#bar")
    .append("svg")
    .attr("width", this.width + (this.margin * 2))
    .attr("height", this.height + (this.margin * 2))
    .append("g")
    .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
  }

  private drawBars(data: any[]): void {
    this.svg.selectAll('*').remove();

    // Create the X-axis band scale
    const x = d3.scaleBand()
    .range([0, this.width])
    .domain(this.groupedVenues.map(d => this.venuesTypes.find(type => 
      type._id == d._id
    )!.name))
    .padding(0.2);

    // Draw the X-axis on the DOM
    this.svg.append("g")
    .attr("transform", "translate(0," + this.height + ")")
    .call(d3.axisBottom(x))
    .selectAll("text")
    .attr("transform", "translate(-10,0)rotate(-45)")
    .style("text-anchor", "end");

    const biggestGroupSize :Number = this.groupedVenues.reduce((acc, x) => {
      if (x.count > acc) return x.count;
      return acc;
    }, 0)
    // Create the Y-axis band scale
    
    const y = d3.scaleLinear()
    .domain([0, biggestGroupSize])
    .range([this.height, 0]);

    // Draw the Y-axis on the DOM
    this.svg.append("g")
    .call(d3.axisLeft(y));

    // Create and fill the bars
    this.svg.selectAll("bars")
    .data(this.groupedVenues)
    .enter()
    .append("rect")
    .attr("x", (d: { _id: string; }) => x(this.venuesTypes.find(type => 
      type._id == d._id
    )!.name))
    .attr("y", (d: { count: d3.NumberValue; }) => y(d.count))
    .attr("width", x.bandwidth())
    .attr("height", (d: { count: d3.NumberValue; }) => this.height - y(d.count))
    .attr("fill", "#d04a35");
  }
}