import { Venue } from './../venue/venue';
import { Component, OnInit, ViewChild } from '@angular/core';
import { VenuesService } from '../venues.service';
import { MatDialogRef } from '@angular/material/dialog';
import { VenueType } from '../venue/venueType';

@Component({
  selector: 'add-venue-dialog',
  templateUrl: './add-venue-dialog.component.html',
  styleUrls: ['./add-venue-dialog.component.css'],
})
export class AddVenueDialogComponent implements OnInit {
  @ViewChild('name') name: any;
  @ViewChild('type') type: any;
  @ViewChild('rating') rating: any;
  @ViewChild('address') address: any;
  venuesTypes: VenueType[] = [];

  ratings: number[] = [1, 2, 3, 4, 5];
  constructor(
    private venuesService: VenuesService,
    public dialogRef: MatDialogRef<AddVenueDialogComponent>
  ) {}

  ngOnInit(): void {
    this.venuesService.venuesTypes.subscribe(
      (venuesTypes) => (this.venuesTypes = venuesTypes)
    );
  }

  onAdd() {
    const venue = {
      name: this.name.nativeElement.value,
      type: this.type.nativeElement.value,
      rating: parseInt(this.rating.nativeElement.value),
      address: this.address.nativeElement.value,
    };

    this.venuesService.addVenue(venue);
    this.dialogRef.close()
  }
}
