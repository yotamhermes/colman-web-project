import requests
from bs4 import BeautifulSoup
import os


def get_all_venues(file_name):
    f = open(file_name, encoding="utf8")
    content = f.read()
    soup = BeautifulSoup(content, "html.parser")
    venues_html = soup.findAll('div', 'biz-inner')

    venues = []

    for venue_html in venues_html:
        rating = venue_html.find('span', 'biz-list-rating')

        if rating is None:
            rating = 2
        else:
            rating = rating.text

        rating = round(float(rating) / 2)
        name = venue_html.find('p', 'biz-list-name').text
        venue_type = venue_html.find('p', 'biz-list-class').text.split('•')[0]
        address = venue_html.find('p', 'biz-list-address').text

        venues.append({
            "name": name,
            "rating": rating,
            "address": address,
            "type": venue_type
        })

    return venues


for file in os.listdir("htmls"):
    for venue in get_all_venues('htmls/' + file):
        url = 'http://localhost:3001/venues/scraping'

        requests.post(url, data=venue)

        print(venue['name'], 'Added')



