var express = require("express");
var router = express.Router();
var database = require("../database.js");
var Venue = require("../models/venue.js").Venue;
var VenueType = require("../models/venueType.js").VenueType;
var mongoose = require("mongoose");

router.delete("/", async function (req, res, next) {
  var _id = req.query._id;
  await Venue.deleteOne({ _id: _id });

  res.send();
});

router.put("/", async function (req, res, next) {
  updatedVenue = req.body;
  var updatedVenue = await Venue.updateOne(
    { _id: updatedVenue._id },
    updatedVenue
  );
  res.send(updatedVenue);
});

router.post("/", async function (req, res, next) {
  var newVenue = new Venue(req.body);
  newVenue.save(function (err) {
    res.send();
  });
});

/* GET venues listing. */
router.get("/", async function (req, res, next) {
  filterParams = {};
  const filterType = req.query.type;
  const filterSearch = req.query.search;
  const filterRating = req.query.rating;

  if (filterType) {
    filterParams.type = filterType;
  }

  if (filterSearch) {
    const regex = new RegExp(filterSearch, "i"); // i for case insensitive
    filterParams.name = { $regex: regex };
  }

  if (filterRating) {
    filterParams.rating = filterRating;
  }

  var venues = await Venue.find(filterParams).populate("type").lean();

  res.send(venues);
});

/* GET venues listing. */
router.get("/types", async function (req, res, next) {
  var venueTypes = await VenueType.find().lean();
  res.send(venueTypes);
});

router.get("/groupedVenues", async function (req, res, next) {
  var groupByField = req.query.groupByField;
  Venue.aggregate(
    [
      {
        $group: {
          _id: "$" + groupByField,
          count: { $sum: 1 },
        },
      },
    ],
    function (err, results) {
      if (err) throw err;
      res.send(results);
    }
  );
});

router.post("/scraping", async function (req, res, next) {
  var typeName = req.body.type;
  var typeFromDB = await VenueType.find({ name: typeName }).lean();

  if (typeFromDB.length == 0) {
    var typeId = mongoose.Types.ObjectId();
    var newVenueType = new VenueType({ name: typeName, _id: typeId });
    typeFromDB = await newVenueType.save();
  } else {
    typeId = typeFromDB[0]._id;
  }

  var name = req.body.name;
  var rating = req.body.rating;
  var address = req.body.address;

  var venueFromDB = await Venue.find({ name: name }).lean();

  if (venueFromDB.length == 0) {
    var newVenue = new Venue({
      name: name,
      rating: rating,
      address: address,
      type: typeId,
    });
    newVenue.save(function (err) {
      if (err) throw err;
      res.send();
    });
  } else {
    res.send();
  }
});

module.exports = router;
