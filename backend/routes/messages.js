var express = require("express");
var router = express.Router();
var database = require("../database.js");
var Message = require("../models/message.js").Message;
var createCountMinSketch = require("count-min-sketch");
const fs = require("fs");

let sketch = createCountMinSketch();

const initiateIO = (io) => {
  io.on("connection", (socket) => {
    socket.on("message", async (msg) => {
      if (msg.message) {
        message = new Message(msg);
        message = await message.save();

        words = msg.message.split(" ");

        words.map((word) => sketch.update(word.toLowerCase(), 1));

        const sketchObj = JSON.stringify(sketch.toJSON());

        fs.writeFileSync("CMS.json", sketchObj);
      }

      io.sockets.emit("message", message);
    });
  });
};

router.delete("/", async function (req, res, next) {
  var _id = req.query._id;
  await Message.deleteOne({ _id: _id });

  res.send();
});

router.post("/", async function (req, res, next) {
  var newMessage = new Message(req.body);
  newMessage = await newMessage.save();

  words = newMessage.split(" ");

  words.map((word) => sketch.update(word, 1));

  res.send(newMessage);
});

router.get("/", async function (req, res, next) {
  const filterSearch = req.query.search;
  const filterSender = req.query.sender;
  filterParams = {};

  if (filterSearch) {
    const regex = new RegExp(filterSearch, "i");
    filterParams.message = { $regex: regex };
  }

  if (filterSender) {
    const regex = new RegExp(filterSender, "i");
    filterParams.sender = { $regex: regex };
  }

  var messages = await Message.find(filterParams).lean();
  res.send(messages);
});

router.get("/word-frequency", (req, res, next) => {
  const word = req.query.word;

  try {
    let rawdata = fs.readFileSync("CMS.json");
    let sketchObj = JSON.parse(rawdata);
    sketch = sketch.fromJSON(sketchObj);

    const count = sketch.query(word.toLowerCase());

    res.send({
      count,
    });
  } catch (error) {
    console.log(error);
  }
});

module.exports = { router, initiateIO };
