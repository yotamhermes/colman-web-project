var mongoose = require('mongoose');
const { Schema } = mongoose;

const venueTypeSchema = new Schema({
    name: String
});

var VenueType = mongoose.model('VenueType', venueTypeSchema);

module.exports = { VenueType: VenueType };
