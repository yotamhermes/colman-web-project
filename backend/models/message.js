var mongoose = require('mongoose');
const { Schema } = mongoose;

const messageSchema = new Schema({
    sender: String,
    message: String,
    timestamp: Date
});

var Message = mongoose.model('Message', messageSchema);

module.exports = { Message: Message };