var mongoose = require('mongoose');
const { Schema } = mongoose;

const venueSchema = new Schema({
    name: String,
    rating: Number,
    address: String,
    type: { type: Schema.Types.ObjectId, ref: "VenueType"}
});

var Venue = mongoose.model('Venue', venueSchema);

module.exports = { Venue: Venue };
