var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");
const http = require("http");
const { Server } = require("socket.io");

var indexRouter = require("./routes/index");
var venuesRouter = require("./routes/venues");
var messagesRouter = require("./routes/messages").router;
var initiateIO = require('./routes/messages').initiateIO
var app = express();
app.use(cors());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/venues", venuesRouter);
app.use("/messages", messagesRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

const server = http.createServer(app);

server.listen(3001, () => {
  console.log("listening on port 3001");
});

const io = new Server(server, {
  cors: {
    origin: "*",
    methods: "*",
  },
});

initiateIO(io);

module.exports = app;
